from setuptools import setup, find_packages
from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))
setup(
    name='bcmuj',
    version='0.1.dev0',
    description='Data collection on Mujoco environment',
    packages=find_packages(),
)
