import itertools
import numpy as np


class ScriptAgent:
    def __init__(self, script, dt):
        self._script = script
        self._idx_script = 0
        self._gen = itertools.chain([])
        self._dt = dt
        self._acc = False
        self._it_acc = 0

    def reset(self):
        self._idx_script = 0
        self._gen = None

    def compute_skill(self, skill, pos_gripper):
        name = skill[0]
        attributes = skill[1]

        if name == 'grip_open':
            for i in range(5):
                yield dict(linear_velocity=np.zeros(3), grip_velocity=-1)
        elif name == 'grip_close':
            for i in range(5):
                yield dict(linear_velocity=np.zeros(3), grip_velocity=-1)
        elif name == 'move_tool':
            max_v = 1
            t_acc = 0.01
            dt = self._dt

            pos_target = attributes[0]
            grip_velocity = int(attributes[1] == 'open')*2-1
            dist = np.subtract(pos_target, pos_gripper)

            t_dec = np.linalg.norm(dist) / max_v
            t_acc = np.min([t_acc, t_dec])
            t_end = t_dec + t_acc
            v_max = dist / t_dec

            if not self._acc:
                self._it_acc = 0
                self._acc = True
                for t in np.arange(0.0, t_end, dt) + dt:
                    k = 1.0
                    if t > t_end:
                        k = 0.0
                    elif t <= t_acc:
                        k = t / t_acc
                    elif t >= t_dec:
                        break
                    yield dict(linear_velocity=v_max * k,
                               grip_velocity=grip_velocity)
            else:
                self._it_acc += 1
                for t in np.arange(0.0, t_end, dt) + dt:
                    k = 1.0
                    if t >= t_dec:
                        k = 1 - (t - t_dec) / t_acc
                    yield dict(linear_velocity=v_max * k,
                               grip_velocity=grip_velocity)
                dist = np.subtract(pos_target, pos_gripper)
                # print('end', np.linalg.norm(dist))
                if np.linalg.norm(dist) < 0.02 or self._it_acc > 4:
                    self._acc = False

    def get_action(self, obs):
        pos_gripper = obs['pos_gripper']
        action = next(self._gen, None)
        if action is None:
            if not self._acc:
                self._idx_script += 1
            if self._idx_script < len(self._script):
                skill = self._script[self._idx_script]
                self._gen = self.compute_skill(skill, pos_gripper)
                action = next(self._gen, None)
            else:
                action = None
        return action
