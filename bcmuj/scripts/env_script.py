import numpy as np
from .utils import move_tool, grip_close, grip_open

'''# expert script
def script_pick_place(pos_gripper, pos_cube, desired_goal, dt):
    script = [grip_open(),
            move_tool(pos_gripper, pos_cube+[0.0, 0.0, 0.1], grip_velocity=1, dt=dt, factor_length=1.25),
            move_tool(pos_gripper, pos_cube+[0.0, 0.0, 0.02], grip_velocity=1, dt=dt, factor_length=1.25),
            grip_close(),
            move_tool(pos_gripper, desired_goal, grip_velocity=-1, dt=dt, factor_length=1.4),]
    return script'''

def script_pick_place(pos_gripper, pos_cube, pos_goal):
    script = [('grip_open', None),
              ('move_tool', (pos_cube+[0, 0, 0.1], 'open')),
              ('move_tool', (pos_cube+[0, 0, 0.0], 'open')),
              ('grip_close', None),
              ('move_tool', (pos_goal, 'close')),
              ]
    return script