import numpy as np

# robot expert primitives
def move_tool(pos_gripper, pos_target, grip_velocity, dt, t_acc=0.01, max_v=1, factor_length=1.25):
        # dist = np.subtract(pos_target, pos_gripper)*factor_length
        dist = np.subtract(pos_target, pos_gripper)

        t_dec = np.linalg.norm(dist) / max_v
        t_acc = np.min([t_acc, t_dec])
        t_end = t_dec + t_acc
        v_max = dist / t_dec

        for t in np.arange(0.0, t_end, dt) + dt:
            k = 1.0
            if t > t_end:
                k = 0.0
            elif t <= t_acc:
                k = t / t_acc
            elif t >= t_dec:
                k = 1 - (t - t_dec) / t_acc
            yield dict(linear_velocity= v_max * k, grip_velocity=grip_velocity)
        # yield dict(linear_velocity=(pos_target-pos_gripper)*10, grip_velocity=grip_velocity)
            
def grip_close():
    for i in range(5):
        yield dict(grip_velocity= -1)
        
def grip_open():
    for i in range(5):
        yield dict(grip_velocity= 1)