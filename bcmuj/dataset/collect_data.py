import os
import time
from tqdm import tqdm
import click
from joblib import Parallel, delayed

from bc.dataset.utils import gather_dataset
from bc.utils import Report

from .collector import collect_trajectory
from ..scripts.env_script import script_pick_place

# defining environment
env_name = 'FetchPickAndPlace-v1'
env_script = script_pick_place
# dataset_dir = '/sequoia/data2/rstrudel/lmdb_mujoco/pick_place_3'
# dataset_dir = '/media/hdd/lmdb_mujoco/pick_place_2'

@click.command()
@click.option('--agent', '-agent', type=str, required=True)
@click.option('--dataset', '-dataset', type=str, default='')
@click.option('--seed_init', '-seed', type=int, default=0)
@click.option('--episodes', '-e', type=int, default=1)
@click.option('--cameras', '-cam', type=int, default=1)
@click.option('--net_path', '-np', type=str, default='')
@click.option('--archi', '-a', type=str, default='resnet18')
@click.option('--first_epoch', '-fe', type=int, default=2)
@click.option('--last_epoch', '-le', type=int, default=3)
@click.option('--iter_epoch', '-ie', type=int, default=2)
@click.option('--report_path', '-rep', type=str, default='')
@click.option('--video_path', '-video', type=str, default='')
@click.option('--processes', '-n', type=int, default=1)
def main(agent, dataset, cameras, seed_init, episodes, net_path, archi,
         first_epoch, last_epoch, iter_epoch, report_path,
         video_path, processes):
    if net_path:
        dir_idx = net_path.rfind('/')
        net_dir = net_path[:dir_idx]
        net_name = net_path[dir_idx+1:]

        # report_path = '/sequoia/data1/rstrudel/reports_mujoco/{}.rep'\
        #               .format(net_name)
        assert report_path is not ''
        if os.path.exists(report_path):
            print('Existing report.')
            report = Report(path=report_path)
        else:
            report = Report(env_name, net_path)

    print('Agent {}'.format(agent))
    print('Net {}'.format(net_path))
    print('Seed {}-{}'.format(seed_init, seed_init+episodes))
    print('Cameras {}'.format(cameras))

    compute = []
    for epoch in range(first_epoch, last_epoch, iter_epoch):
        for seed in range(seed_init, seed_init+episodes):
            if net_path:
                if not report.is_entry(epoch, seed):
                    compute.append((epoch, seed))
            else:
                compute.append((epoch, seed))
    t0 = time.time()

    if not os.path.exists(dataset) and agent == 'script':
        os.mkdir(dataset)

    results = Parallel(n_jobs=processes)(
        delayed(collect_trajectory)(env_name, env_script, seed, agent, dataset,
                                    net_path, archi, epoch, cameras, video_path)
        for epoch, seed in tqdm(compute))
    # results = collect_trajectory(env_name, env_script, 0, agent, dataset,
    #                              net_path, archi, epoch, cameras)

    if net_path:
        for seed, success, epoch in results:
            report.add_entry(seed, epoch, success)

        print(report.get_success_rate())
        report.save(report_path)
    else:
        print('Gathering trajectories dataset into one dataset...')
        gather_dataset(dataset)

    print('Trajectories {}-{} collected in {:.2f} seconds.'.format
          (seed_init, seed_init+episodes-1, time.time()-t0))

if __name__ == '__main__':
    main()
