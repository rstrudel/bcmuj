import os
import numpy as np
import pickle as pkl

import gym
from gym.utils import seeding

from bc.agent import NetAgent
from bc.dataset.dataset_lmdb import DatasetWriter
from bc.dataset.utils import compress_images, process_trajectory
from bc.utils import videos

from ..scripts import ScriptAgent
from ..scene import Camera


def collect_trajectory(env_name, env_script, seed, agent_type, path_dataset,
                       net_path=None, archi='', net_epoch=-1, num_cameras=1,
                       video_path='', resolution=(224, 224)):
    # initialize dataset
    if agent_type == 'script':
        path_worker_dataset = os.path.join(path_dataset, '{:06}'.format(seed))
        dataset = DatasetWriter(path_worker_dataset, env_name, rewrite=True,
                                float_depth=False)
        scalars = {}

    # initialize environment
    env = gym.make(env_name)
    env.seed(seed)
    np_random, seed = seeding.np_random(seed)
    dic_init = env.reset()

    # camera
    cam = Camera(dr=(0, 0.1), dtheta=(-np.pi/3, np.pi/3),
                 dphi=(np.pi/20, np.pi/6), np_random=np_random)
    cam_poses = []
    for i in range(num_cameras):
        pos, quat = cam.sample_pose()
        cam_poses.append((pos, quat))

    sim = env.unwrapped.sim
    dt = sim.nsubsteps*sim.model.opt.timestep
    factor = 4

    # setting desired goal to the right position
    sites_offset = (sim.data.site_xpos - sim.model.site_pos).copy()
    site_id = sim.model.site_name2id('target0')
    sim.model.site_pos[site_id] = env.unwrapped.goal - sites_offset[0]
    sim.forward()

    # position of objects in the scene
    pos_gripper = sim.data.get_site_xpos('robot0:grip')
    pos_cube = sim.data.get_site_xpos('object0')
    desired_goal = dic_init['desired_goal']

    # expert script and agent
    if agent_type == 'script':
        script = env_script(pos_gripper, pos_cube, desired_goal)
        agent = ScriptAgent(script, dt/factor)
    else:
        agent = NetAgent(archi=archi, channels='rgbd', path=net_path,
                         num_frames=3, skip=1, max_steps=300, epoch=net_epoch,
                         action_space='tool', dim_action=4, steps_action=4,
                         num_skills=1)

    states = []
    actions = []
    # action = agent.get_action()
    action = 0
    success = False
    while action is not None and (agent_type == 'script' or not success):
        # t0 = time.time()
        obs = {}
        for i, (cam_pos, cam_quat) in enumerate(cam_poses):
            sim.model.cam_pos[-1] = cam_pos
            sim.model.cam_quat[-1] = cam_quat
            sim.forward()
            rgb, depth = sim.render(resolution[0], resolution[1], depth=True,
                                    camera_name='external_camera_0')
            rgb = rgb[::-1]
            depth = (depth[::-1]*255).astype(np.uint8)
            obs['rgb{}'.format(i)] = rgb
            obs['depth{}'.format(i)] = depth
            obs['pos_gripper'] = sim.data.get_site_xpos('robot0:grip')
        if agent_type == 'script':
            compress_images(obs)
        action = agent.get_action(obs)
        # action = agent.get_action(None)
        if action is not None:
            states.append(obs)
            actions.append(action)
            dx = np.zeros(4)
            dx[:3] = action['linear_velocity']/factor
            dx[3] = action['grip_velocity']/2
            _, reward, done, dic_success = env.step(dx)
        success = dic_success['is_success'] > 0.99

    if success and agent_type == 'script':
        frames_chunk, scalars_chunk = process_trajectory(
            states, actions, range(len(states)),
            seed, jpeg_compression=False)
        dataset.init_db()
        dataset.write_frames(frames_chunk)
        dataset.close_db()
        scalars.update(scalars_chunk)
        pkl.dump(scalars, open(os.path.join(path_dataset,
                                            '{:06}.pkl'.format(seed)), 'wb'))
    elif agent_type == 'net':
        videos.write_video([obs['rgb'] for obs in states],
                           os.path.join(video_path,'{:06}_rgb.gif'.format(seed)))

    return seed, success, net_epoch
