import numpy as np
from pyquaternion import Quaternion

# all angles are defined using radians

class Camera:
    def __init__(self, radius=1, theta=0, phi=0, dr=0, dtheta=(0, 0), dphi=(0, 0), sphere_center=[0.5, 0.0, 0.5], np_random=np.random):
        self.sphere_center = sphere_center

        q0 = Quaternion([np.cos(np.pi/4), 0, 0, np.sin(np.pi/4)])
        q1 = Quaternion([np.cos(np.pi/4), 0, np.sin(np.pi/4), 0])
        self.default_ref = q1*q0
        self.radius = radius
        self.theta = theta
        self.phi = phi
        self.dr = dr
        self.dtheta = dtheta
        self.dphi = dphi

        self.np_random = np_random

    def get_absolute_pose(self, radius, theta, phi):
        u = np.array([np.cos(phi/2), 0., -np.sin(phi/2), 0.])
        v = np.array([np.cos(theta/2), 0, 0., np.sin(theta/2)])

        cam_pos = self.sphere_center+self.sph2pos(radius, theta, phi)
        q = Quaternion(v)*Quaternion(u)*self.default_ref

        return cam_pos.copy(), self.q2arr(q).copy()

    def get_pose(self, dr, dtheta, dphi):
        return self.get_absolute_pose(self.radius+dr, self.theta+dtheta, self.phi+dphi)

    def sample_pose(self):
        np_random = self.np_random
        dr = self.dr
        dtheta = self.dtheta
        dphi = self.dphi

        radius = self.radius+np_random.uniform(-0.1, 0.1)
        theta = self.theta+np_random.uniform(dtheta[0], dtheta[1])
        phi = self.phi+np_random.uniform(dphi[0], dphi[1])

        return self.get_absolute_pose(radius, theta, phi)

    def sph2pos(self, r, theta, phi):
        x = r*np.cos(phi)*np.cos(theta)
        y = r*np.cos(phi)*np.sin(theta)
        z = r*np.sin(phi)
        return np.array([x, y, z])

    def q2arr(self, q):
        return np.array([q[i] for i in range(4)])
