# Mujoco

This repository provides tools to collect demonstrations in OpenAI `gym` robotics environment.
Make sure you installed the `bc` repository.

## Install the repository

Clone the repository, go to the root of the repository and use `pip install -e .`.

## How to collect a dataset

To collect a dataset, run the command :
```
python -m bcmuj.dataset.collect_data --agent script --dataset <dataset_path>
--seed 0 --episodes 100 --processes 8
```

This command collects a dataset of 100 demonstrations. For each demonstrations, the environment 
is randomly initialized according to the `seed`. To perform a rollout, we use a script `agent` able 
to perform the task. You can define the number of processes running in parallel to collect data 
by changing `processes`.

## How to test an agent

Once you trained an agent with the `bc` repository, you can test checkpoints stored during training with the following command.

```
python -m bcmuj.dataset.collect_data --agent net --net_path <net_path> --seed 50000 --episodes 100
--first_epoch 2 --last_epoch 12 --iter_epoch 4 --report_path <report_path> --video_path <video_path> --processes 8
```

This command runs the learned policy in the environment and compute its success rate. `(first_epoch,
last_epoch, iter_epoch)` defines the range of epochs to be tested. Once the script is done, a dictionnary with the epoch number and its success rate is printed. The success rate of the tested 
checkpoints are stored into a `Report` file that can be loaded afterwards to check 
which seeds failed for example. During policy evaluation, gifs will be generated in the directory `video_path`.\\ 

To test your policy be careful about choosing seeds you did not use for training.

## Mujoco lock issue

Sometimes, if you abort the script during data collection, `mujoco` gets stuck and data collection 
is pending (nothing is hapenning) when you re-execture the script. In this case, go to your python
root and remove the file `lib/python3.6/site-packages/mujoco_py/generated/mujocopy-buildlock.lock`.
